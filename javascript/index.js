function createGrid (numOfRows, numOfColumns) {
    const gridContainer = document.getElementById('grid-container');

    gridContainer.style.setProperty('--grid-rows', numOfRows);
    gridContainer.style.setProperty('--grid-columns', numOfColumns);

    for (i = 0; i < (numOfRows * numOfColumns); i++) {
        const cell = document.createElement('div');

        cell.style.setProperty('width', '100%');
        cell.style.setProperty('height', '2rem');
        gridContainer.appendChild(cell).className = 'grid-item';
    }

}

function setHoverEvent () {
    const gridItems = document.querySelectorAll('.grid-item');
    let randomColorR = Math.floor(Math.random() * 255);
    let randomColorG = Math.floor(Math.random() * 255);
    let randomColorB = Math.floor(Math.random() * 255);



    for (i = 0; i < gridItems.length; i++) {
        gridItems[i].addEventListener('mouseover', (e) => {
            e.target.style.setProperty('background-color', `rgb(${randomColorR}, ${randomColorG}, ${randomColorB})`);
        });
    }
}

function clearGrid () {
    const gridItems = document.querySelectorAll('.grid-item');

    for (i = 0; i < gridItems.length; i++) {
        gridItems[i].style.setProperty('background-color', 'white');
    }
    
}

function clearAndRecreateGrid() {
    let numOfRows = prompt("Enter number of rows:");
    let numofColumns = prompt("Enter number of columns:");

    clearGrid();
    createGrid(parseInt(numOfRows, 10), parseInt(numofColumns, 10));
    setHoverEvent();
}

createGrid(16, 16);
setHoverEvent();